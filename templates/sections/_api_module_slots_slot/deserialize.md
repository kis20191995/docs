---
source: sections/_api_module_slots_slot/deserialize.md
title: deserialize
bookmark: deserialize
description: Deserializes to a slot instance.
endpoint: method
signature: true
---

#### Signature
##### **deserialize**(slot: *ISlot\<T\>*): *this*
{: .signature }

`slot` ISlot\<T\>
: Contains the serialized slot.

#### Returns
Returns a reference to the instance.
