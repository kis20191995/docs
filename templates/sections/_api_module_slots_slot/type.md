---
source: sections/_api_module_slots_slot/type.md
title: type
bookmark: type
description: Retrieves the slot type.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **type**(): *string*
{: .signature }
