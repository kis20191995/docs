---
source: sections/_api_builder_forms_button/type.md
title: type
bookmark: type
description: Sets the type.
endpoint: method
signature: true
---

#### Signature
##### **type**(type: *Types*): *this*
{: .signature }

`type` Types
: Specifies the type.

#### Returns
Returns a reference to the control to allow chaining.
