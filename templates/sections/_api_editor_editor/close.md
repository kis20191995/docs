---
source: sections/_api_builder_builder/close.md
title: close
bookmark: close
description: Closes the builder.
endpoint: method
signature: true
---

#### Signature
##### **close**(): *this*
