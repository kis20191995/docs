---
source: sections/_api_builder_builder/onClose.md
title: onClose
bookmark: on-close
description: Invoked when the builder is closed.
endpoint: method
signature: true
---

#### Signature
##### **onClose**(): *(pBuilder: Builder) => void*
{: .signature }
