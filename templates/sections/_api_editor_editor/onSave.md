---
source: sections/_api_builder_builder/onSave.md
title: onSave
bookmark: on-save
description: Invoked when the definition is saved.
endpoint: method
signature: true
---

#### Signature
##### **onSave**(): *(pDefinition: IDefinition, pBuilder: Builder) => void*
{: .signature }
