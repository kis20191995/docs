---
source: sections/_api_builder_forms_numeric/fill.md
title: fill
bookmark: fill
description: Fills out the number with leading zero's till the desired length is reached.
endpoint: method
signature: true
---

#### Signature
##### **fill**(fill: *number*): *this*
{: .signature }

`fill` number
: Specifies the option value.

#### Returns
Returns a reference to the control to allow chaining.
