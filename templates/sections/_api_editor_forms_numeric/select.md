---
source: sections/_api_builder_forms_numeric/select.md
title: select
bookmark: select
description: Selects the number.
endpoint: method
signature: true
---

#### Signature
##### **select**(): *this*
{: .signature }

#### Returns
Returns a reference to the control to allow chaining.
