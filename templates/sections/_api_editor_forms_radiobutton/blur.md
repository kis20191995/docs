---
source: sections/_api_builder_forms_radiobutton/blur.md
title: blur
bookmark: blur
description: Blurs the focus.
endpoint: method
signature: true
---

#### Signature
##### **blur**(): *void*
{: .signature }

#### Returns
Nothing
