---
source: sections/_api_module_slots_slots/constructor.md
title: constructor
bookmark: constructor
description: Constructs a new slots instance.
endpoint: constructor
signature: true
---

#### Signature
##### new **Slots**(): *Slots*
{: .signature }

#### Returns
`Slots`
