---
source: sections/_api_builder_templates_label/options.md
title: options
bookmark: options
description: Lorem ipsum.
endpoint: method
signature: true
---

#### Signature
##### **options**(controller: *BuilderController*): *Feature*
{: .signature }

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
