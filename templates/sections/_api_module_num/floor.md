---
source: sections/_api_module_num/floor.md
title: floor
bookmark: floor
description: Round a floating point number downward to its nearest integer.
endpoint: function
signature: true
code: |
  ``` typescript
  floor(1.6); // Returns `1`
  ```
---

#### Signature
##### Num.**floor**(value: *number*): *number*
{: .signature }

`value` number
: Input number.

#### Returns
Returns the floored number.
