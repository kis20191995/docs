---
source: sections/_api_module_num/max.md
title: max
bookmark: max
description: Compares two numbers and returns the highest value.
endpoint: function
signature: true
code: |
  ``` typescript
  max(1, 2); // Returns `2`
  ```
---

#### Signature
##### Num.**max**(a: *number*, b: *number*): *number*
{: .signature }

`a` number
: Input number A.

`b` number
: Input number B.

#### Returns
Returns the number with the highest value.
