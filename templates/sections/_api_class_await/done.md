---
source: sections/_api_class_await/done.md
title: done
bookmark: done
description: Terminates the await.
endpoint: method
signature: true
---

#### Signature
##### **done**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the await loop is terminated.
