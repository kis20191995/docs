---
source: sections/_api_functions/cancelTimeout.md
title: cancelTimeout
bookmark: cancel-timeout
description: Cancels a pending asynchronous call.
endpoint: function
signature: true
---

#### Signature
##### **cancelTimeout**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the asynchronous timeout call.

#### Returns
Always returns 0.
