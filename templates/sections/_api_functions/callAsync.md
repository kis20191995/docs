---
source: sections/_api_functions/callAsync.md
title: callAsync
bookmark: call-async
description: Calls the supplied function asynchronous.
endpoint: function
signature: true
---

#### Signature
##### **callAsync**(callee: *function | undefined*, ...arguments: *any[]*): *void*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
