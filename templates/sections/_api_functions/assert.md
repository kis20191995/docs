---
source: sections/_api_functions/assert.md
title: assert
bookmark: assert
description: Verifies if the supplied value is defined. If not, an error is thrown.
endpoint: function
signature: true
---

#### Signature
##### **assert**\<T\>(assert: *T | undefined*, error?: *string*): *T*
{: .signature }

`assert` T | undefined
: Specifies the value to assert.

`error` Optional string
: Specifies the optional error message that is thrown.

#### Returns
Returns the value.
