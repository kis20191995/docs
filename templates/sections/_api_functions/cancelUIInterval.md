---
source: sections/_api_functions/cancelUIInterval.md
title: cancelUIInterval
bookmark: cancel-ui-interval
description: Cancels a pending UI interval call.
endpoint: function
signature: true
---

#### Signature
##### **cancelUIInterval**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the asynchronous interval call.

#### Returns
Always returns 0.
