---
source: sections/_api_functions/castToBoolean.md
title: castToBoolean
bookmark: cast-to-boolean
description: Cast a variable to a boolean value.
endpoint: function
signature: true
code: |
  ``` typescript
  castToBoolean(1); // Returns `true`
  castToBoolean("true"); // Returns `true`
  castToBoolean("false"); // Returns `false`
  ```
---

#### Signature
##### **castToBoolean**(value: *any*, default?: *boolean*): *boolean*
{: .signature }

`value` any
: Source variable.

`default` Optional boolean
: Optional parameter which specifies the default boolean value if the supplied source variable cannot be casted.

#### Returns
Returns the boolean value.
