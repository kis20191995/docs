---
source: sections/_api_functions/setAny.md
title: setAny
bookmark: set-any
description: Sets the value of the specified variable in the object array.
endpoint: function
signature: true
  ```
---

#### Signature
##### **setAny**\<T\>(object: *any*, name: *string*, value: *T*): *T*
{: .signature }

`object` any
: Reference to the object.

`name` string
: Name of the variable.

`value` T
: Value for the variable.

#### Returns
Returns a reference to the value on the object.
