---
source: sections/_api_functions/getFloat.md
title: getFloat
bookmark: get-float
description: Retrieves a floating value of the specified variable in the object array.
endpoint: function
signature: true
---

#### Signature
##### **getFloat**\<T\>(object: *IObject | undefined*, name: *string*, default?: *number*): *number*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

`default` Optional number
: Specifies the default value (defaults to `0` if omitted).

#### Returns
Returns the value of the variable.
