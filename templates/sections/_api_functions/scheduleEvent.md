---
source: sections/_api_functions/scheduleEvent.md
title: scheduleEvent
bookmark: schedule-event
description: Calls the supplied event function (after 2 animation frames).
endpoint: function
signature: true
---

#### Signature
##### **scheduleEvent**(event: *function | undefined*, ...arguments: *any[]*): *void*
{: .signature }

`event` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Nothing
