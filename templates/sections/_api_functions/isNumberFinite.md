---
source: sections/_api_functions/isNumberFinite.md
title: isNumberFinite
bookmark: is-number-finite
description: Validates if the supplied variable is a finite number and optionally checks if the number is within the specified range.
endpoint: function
signature: true
code: |
  ``` typescript
  isNumberFinite(1); // Returns `true`
  isNumberFinite(Infinity); // Returns `false`
  isNumberFinite(NaN); // Returns `false`
  isNumberFinite("1"); // Returns `false`
  ```
---

#### Signature
##### **isNumberFinite**(num: *any*, rangeLower?: *number*, rangeUpper?: *number*): *boolean*
{: .signature }

`num` any
: Variable to validate.

`rangeLower` Optional number
: Optional parameter which specifies the lower range.

`rangeUpper` Optional number
: Optional parameter which specifies the upper range.

#### Returns
Returns `true` if the variable is a number.
