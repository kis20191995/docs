---
source: sections/_api_functions/cancelTick.md
title: cancelTick
bookmark: cancel-tick
description: Cancels a pending asynchronous tick call.
endpoint: function
signature: true
---

#### Signature
##### **cancelTick**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the call.

#### Returns
Always returns 0.
