---
source: sections/_api_functions/cancelFrame.md
title: cancelFrame
bookmark: cancel-frame
description: Cancels a pending asynchronous frame call.
endpoint: function
signature: true
---

#### Signature
##### **cancelFrame**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the call.

#### Returns
Always returns 0.
