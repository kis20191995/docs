---
source: sections/_api_functions/count.md
title: count
bookmark: count
description: Counts the number of items in the supplied array, collection or enumerable object.
endpoint: function
signature: true
code: |
  ``` typescript
  count<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `2`
  ```
---

#### Signature
##### **count**\<T\>(list: *TList\<T\> | undefined*, truth?: *function*): *number*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` Optional function
: Specifies the optional truth function. Only items that pass the truth test are included in the count. The function should return a boolean value whether the item should be counted. If omitted all items are counted.

#### Returns
Returns the number of items that pass.
