---
source: sections/_api_functions/extend.md
title: extend
bookmark: extend
description: Extend an object by appending all of the properties from each supplied object. When there are identical properties, the right-most property takes precedence. This function returns a mutable object reference which is the supplied `object` (the first function argument).
endpoint: function
signature: true
code: |
  ``` typescript
  extend({ a: 1, b: 2 }, { b: 3, d: 4 }, { d: 5 }); // Returns `{ a: 1, b: 3, d: 5 }`
  ```
---

#### Signature
##### **extend**\<T\>(object: *T*, ...objects: (*undefined | T*)[]): *T*
{: .signature }

`object` T
: Specifies the object to extend.

`objects` (undefined | T)[]
: Specifies the objects to extend with.

#### Returns
Returns the extended mutable object which is a reference to `object`.
