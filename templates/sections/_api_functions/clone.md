---
source: sections/_api_functions/clone.md
title: clone
bookmark: clone
description: Clones an object.
endpoint: function
signature: true
code: |
  ``` typescript
  clone({ a: 1, b: 2 }); // Returns `{ a: 1, b: 2 }`
  ```
---

#### Signature
##### **clone**\<T\>(object: *T*): *T*
{: .signature }

`object` T
: Specifies the object to clone.

#### Returns
Returns a clone of the object which is no reference to the original object.
