---
source: sections/_api_functions/isDate.md
title: isDate
bookmark: is-date
description: Validates if the supplied variable is a date.
endpoint: function
signature: true
---

#### Signature
##### **isDate**(date: *any*): *boolean*
{: .signature }

`date` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a date.
