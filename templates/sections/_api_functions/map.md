---
source: sections/_api_functions/map.md
title: map
bookmark: map
description: Produces a new array of values by mapping each value in the supplied array, collection or enumerable object through a transformation function.
endpoint: function
signature: true
code: |
  ``` typescript
  map<number, number>([1, 2, 3], (nItem: number) => nItem * nItem); // Returns `[1, 4, 9]`
  map<number, number>([1, 2, 3], (nItem: number, nMultiplier: number) => nItem * nMultiplier, 2); // Returns `[2, 4, 6]`
  ```
---

#### Signature
##### **map**\<T, R\>(list: *TList\<T\> | undefined*, transformation: *function*, ...arguments: *any[]*): *R*[]
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`transformation` function
: Specifies the function to be invoked for each element. The element value will be exposed to the function as the first argument of the argument list. Additional arguments can be specified and will be pushed to the function.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns an array with mapped values.
