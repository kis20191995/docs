---
source: sections/_api_functions/getString.md
title: getString
bookmark: get-string
description: Retrieves a string value of the specified variable in the object array.
endpoint: function
signature: true
---

#### Signature
##### **getString**\<T\>(object: *IObject | undefined*, name: *string*, default?: *string*): *string*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

`default` Optional string
: Specifies the default value (defaults to `""` if omitted).

#### Returns
Returns the value of the variable.
