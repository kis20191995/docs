---
source: sections/_api_functions/call.md
title: call
bookmark: call
description: Calls the supplied function.
endpoint: function
signature: true
---

#### Signature
##### **call**\<T\>(callee: *function | undefined*, ...arguments: *any[]*): *T | undefined*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns `undefined` if the call fails or the return value of the function which is executed if the call succeeded.
