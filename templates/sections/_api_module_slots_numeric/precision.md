---
source: sections/_api_module_slots_numeric/precision.md
title: precision
bookmark: precision
description: Retrieves or specifies the numeric precision.
endpoint: property
signature: true
---

#### Signature
##### **precision**: *number*
{: .signature }
