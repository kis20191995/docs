---
source: sections/_api_module_slots_numeric/prefix.md
title: prefix
bookmark: prefix
description: Contains the prefix.
endpoint: property
signature: true
---

#### Signature
##### **prefix**?: *string*
