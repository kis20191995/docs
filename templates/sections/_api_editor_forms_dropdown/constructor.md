---
source: sections/_api_builder_forms_dropdown/constructor.md
title: constructor
bookmark: constructor
description: Creates a new dropdown instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Dropdown**(options: *TOption\<T\>[] | function*, value: *TBinding\<T\>*, style?: *IDropdownStyle*): *Dropdown*
{: .signature }

`options` TOption\<T\>[] | function
: Specifies the options. This can be a callback function which should return the options in a asynchronous matter.

`value` TBinding\<T\>
: Specifies the initially selected value.

`style` Optional IDropdownStyle
: Specifies the optional style.

#### Returns
`Dropdown`
