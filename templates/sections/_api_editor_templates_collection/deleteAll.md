---
source: sections/_api_builder_templates_collection/deleteAll.md
title: deleteAll
bookmark: delete-all
description: Deletes all items from the collection.
endpoint: method
signature: true
---

#### Signature
##### **deleteAll**(): *void*
{: .signature }

#### Returns
Nothing
