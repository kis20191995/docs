---
source: sections/_api_builder_templates_collection/sort.md
title: sort
bookmark: sort
description: Sorts the list with items.
endpoint: method
signature: true
---

#### Signature
##### **sort**(direction: *"ascending" | "descending"*): *void*
{: .signature }

#### Returns
Nothing
