---
source: sections/_api_builder_forms_datetime/hasValue.md
title: hasValue
bookmark: has_value
description: Retrieves if the control has a value.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **hasValue**: *boolean*
{: .signature }
