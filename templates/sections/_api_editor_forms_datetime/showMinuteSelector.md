---
source: sections/_api_builder_forms_datetime/showMinuteSelector.md
title: showMinuteSelector
bookmark: show_minute_selector
description: Shows the minute selector.
endpoint: method
signature: true
---

#### Signature
##### **showMinuteSelector**(): *void*
{: .signature }

#### Returns
Nothing
