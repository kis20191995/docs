---
source: sections/_api_builder_forms_datetime/year.md
title: year
bookmark: year
description: Retrieves or specifies the year.
endpoint: property
signature: true
---

#### Signature
##### **year**: *number*
{: .signature }

