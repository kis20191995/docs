---
source: sections/_api_builder_forms_datetime/showWeekdaySelector.md
title: showWeekdaySelector
bookmark: show_weekday_selector
description: Shows the weekday selector.
endpoint: method
signature: true
---

#### Signature
##### **showWeekdaySelector**(): *void*
{: .signature }

#### Returns
Nothing
