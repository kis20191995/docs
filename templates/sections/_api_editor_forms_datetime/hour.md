---
source: sections/_api_builder_forms_datetime/hour.md
title: hour
bookmark: hour
description: Retrieves or specifies the hour.
endpoint: property
signature: true
---

#### Signature
##### **hour**: *number*
{: .signature }
