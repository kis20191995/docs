---
source: sections/_api_builder_forms_checkbox/on.md
title: "on"
bookmark: "on"
description: Specifies the function which is invoked when the checkbox is toggled.
endpoint: method
signature: true
---

#### Signature
##### **on**(toggle: *function*): *this*
{: .signature }

`toggle` function
: Specifies the toggle function. The `Checkbox` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
