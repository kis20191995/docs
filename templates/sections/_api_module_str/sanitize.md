---
source: sections/_api_module_str/sanitize.md
title: sanitize
bookmark: sanitize
description: Sanitize a string by removing all leading, trailing and multiple whitespaces (`_ab__cd_` -> `ab_cd`).
endpoint: function
signature: true
code: |
  ``` typescript
  sanitize(" ab  cd "); // Returns `ab cd`
  ```
---

#### Signature
##### Str.**sanitize**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the sanitized string.
