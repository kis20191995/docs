---
source: sections/_api_module_str/makeHTMLSafe.md
title: makeHTMLSafe
bookmark: make-html-safe
description: Converts HTML brackets to HTML entities so it is safe to display without HTML parsing. Careful, only the brackets (`<` and `>`) are converted to HTML entities.
endpoint: function
signature: true
code: |
  ``` typescript
  makeHTMLSafe("<b>Hello</b>"); // Returns `&lt;b&gt;Hello&lt;b&gt;`
  ```
---

#### Signature
##### Str.**makeHTMLSafe**(value: *string*): *string*
{: .signature }

`value` string
: Specifies the string to be converted (variable will be casted to a string if necessary).

#### Returns
Returns the converted string.
