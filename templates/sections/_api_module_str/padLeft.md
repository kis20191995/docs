---
source: sections/_api_module_str/padLeft.md
title: padLeft
bookmark: pad-left
description: Converts source variable to a string and pads the string on the left with the supplied number of characters.

endpoint: function
signature: true
code: |
  ``` typescript
  padLeft("ABC", "A", 5); // Returns `AAABC`
  ```
---

#### Signature
##### Str.**padLeft**(value: *string | number*, fill: *string*, length: *number*, crop?: *boolean*, treatAsNumber?: *boolean*): *string*
{: .signature }

`value` string | number
: Specifies the value to pad.

`fill` string
: Contains the string which will be used as fill string.

`length` number
: Specifies the desired string length.

`crop` Optional boolean
: Optional boolean value which enables string cropping if the source string length is larger than the desired string length.

`treatAsNumber` Optional boolean
: Optional boolean value which specifies the input string should be treated as a number.

#### Returns
Returns the padded string.
