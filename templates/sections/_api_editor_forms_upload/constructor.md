---
source: sections/_api_builder_forms_upload/constructor.md
title: constructor
bookmark: constructor
description: Creates a new upload instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Upload**(type?: *"single" | "multiple"*, files?: *IUploadedFile[] | IUploadedFile*, lines?: *number*, style?: *IUploadStyle*): *Upload*
{: .signature }

`type` Optional "single" | "multiple"
: Specifies the upload type (defaults to `single` if omitted).

`files` Optional IUploadedFile[] | IUploadedFile
: Specifies the currently uploaded files.

`style` Optional IUploadStyle
: Specifies the optional style.

#### Returns
`Upload`
