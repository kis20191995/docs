---
source: sections/_api_builder_forms_upload/destroy.md
title: destroy
bookmark: destroy
description: Destroys the control.
endpoint: method
signature: true
---

#### Signature
##### **destroy**(): *void*
{: .signature }

#### Returns
Nothing
