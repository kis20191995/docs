---
source: sections/_api_builder_forms_upload/maxFiles.md
title: maxFiles
bookmark: maxFiles
description: Specifies the maximum number of files that can be uploaded.
endpoint: method
signature: true
---

#### Signature
##### **maxFiles**(max: *number*): *this*
{: .signature }

`max` number
: Specifies the maximum number of files.

#### Returns
Returns a reference to the control to allow chaining.
