---
source: sections/_api_builder_forms_upload/headers.md
title: headers
bookmark: headers
description: Sets the HTTP headers.
endpoint: method
signature: true
---

#### Signature
##### **headers**(headers: *IHTTPHeaders*): *this*
{: .signature }

`headers` IHTTPHeaders

#### Returns
Returns a reference to the control to allow chaining.
