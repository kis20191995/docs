---
source: sections/_api_builder_forms_upload/onListResize.md
title: onListResize
bookmark: on-list-resize
description: Invoked when the file list is resized.
endpoint: method
signature: true
---

#### Signature
##### **onListResize**(): *void*
{: .signature }

#### Returns
Nothing
