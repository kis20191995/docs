---
source: sections/_api_builder_forms_upload/HTTPHeaders.md
title: HTTPHeaders
bookmark: http-headers
description: Retrieves the HTTP headers.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **HTTPHeaders**: *IHTTPHeaders*
{: .signature }
