---
source: sections/_api_builder_forms_upload/file.md
title: file
bookmark: file
description: Retrieves the first uploaded file.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **file**: *IUploadFile | undefined*
{: .signature }
