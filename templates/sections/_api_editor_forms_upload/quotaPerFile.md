---
source: sections/_api_builder_forms_upload/quotaPerFile.md
title: quotaPerFile
bookmark: quota-per-file
description: Specifies the maximum size per file.
endpoint: method
signature: true
---

#### Signature
##### **quotaPerFile**(max: *number*, unit: *"B" | "kB" | "MB" | "GB" | "TB"*): *this*
{: .signature }

`max` number
: Specifies the maximum file size.

`unit` "B" | "kB" | "MB" | "GB" | "TB"

#### Returns
Returns a reference to the control to allow chaining.
