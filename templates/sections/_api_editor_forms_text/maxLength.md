---
source: sections/_api_builder_forms_text/maxLength.md
title: maxLength
bookmark: max-length
description: Specifies the maximum text length.
endpoint: method
signature: true
---

#### Signature
##### **maxLength**\<T\>(maxLength: *number*): *this*
{: .signature }

`maxLength` number
: Specifies the maximum text length.

#### Returns
Returns a reference to the control to allow chaining.
