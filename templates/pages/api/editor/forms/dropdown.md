---
base: ../../../../
permalink: /api/builder/forms/dropdown/
title: Dropdown / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_dropdown | sort: "path" %}

<section>
    <section>
        <h2>
            Dropdown
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form dropdown control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
