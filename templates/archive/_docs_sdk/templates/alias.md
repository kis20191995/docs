
---
---

#### Signature
##### **alias**(slot: *Slot | undefined*, controller: *BuilderController*): *Feature | undefined*
{: .signature }

#### Description
Adds the alias feature.

`slot` Slot | undefined
: Reference to a slot.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
