---
---

#### Signature
##### **deleteAll**(): *void*
{: .signature }

#### Description
Deletes all items from the collection.
