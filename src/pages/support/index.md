---
base: ../
permalink: /support/
source: pages/support/index.md
title: Support - Tripetto Documentation
heading: Need help?
---

### We keep working on Tripetto
This website contains the information needed for developers to start using Tripetto and hopefully even build on our foundation. We’ll use any constructive feedback to further improve the software.

#### More information
Have a look at our [developers page at tripetto.com](https://tripetto.com/developers/){:target="_blank"} for more information about our SDK.

#### Request a SDK license
Request a SDK license by filling out [this form](https://tripetto.app/run/DIAC6PWFZ2){:target="_blank"}.

#### Got a question?
Request support at [tripetto.com](https://tripetto.com/support/){:target="_blank"}.
