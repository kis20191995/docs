---
source: sections/_examples/01-runner.md
title: Runner examples
bookmark: runner
---

#### React + Bootstrap
Uses the [React](https://reactjs.org/){:target="_blank"} library to implement a runner as a reusable component using [Bootstrap](https://getbootstrap.com/) for the UI.

[![Try the demo](../images/demo.svg)](https://example-react-bootstrap.tripetto.com/){:target="_blank"}
[![Get the code](../images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react){:target="_blank"}

---

#### React + Material-UI
Uses the [React](https://reactjs.org/) library to implement a runner as a reusable component using [Material-UI](https://material-ui-next.com/) for the UI.

[![Try the demo](../images/demo.svg)](https://example-react-material-ui.tripetto.com/){:target="_blank"}
[![Get the code](../images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react-material-ui){:target="_blank"}

---

#### Angular + Bootstrap
Uses the [Angular](https://angular.io/){:target="_blank"} framework to implement a reusable component using [Bootstrap](https://getbootstrap.com/) for the UI.

[![Try the demo](../images/demo.svg)](https://example-angular-bootstrap.tripetto.com/){:target="_blank"}
[![Get the code](../images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular){:target="_blank"}

---

#### Angular + Angular Material
Uses the [Angular](https://angular.io/){:target="_blank"} framework with [Angular Material](https://material.angular.io/){:target="_blank"} to implement a reusable component.

[![Try the demo](../images/demo.svg)](https://example-angular-material.tripetto.com/){:target="_blank"}
[![Get the code](../images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular-material){:target="_blank"}

---

#### Conversational UX with React + Bootstrap
Shows how you can use Tripetto to create a stunning conversational look and feel using [React](https://reactjs.org/){:target="_blank"} and [Bootstrap](https://getbootstrap.com/).

[![Try the demo](../images/demo.svg)](https://example-react-conversational.tripetto.com/){:target="_blank"}
[![Get the code](../images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react-conversational){:target="_blank"}
