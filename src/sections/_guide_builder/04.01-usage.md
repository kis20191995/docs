---
source: sections/_guide_builder/04.01-usage.md
bookmark: library-usage
name: Usage
title: Using the library in your project
sub: true
---

First you need to add the library to your project. There are multiple ways to do this, depending on your specific needs. The two most common are the following.

### Method 1: Using npm
If you are familiar with [npm](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}, this is the preferred way to go. It allows you to use modern techniques like ES6 module syntax and code bundling (for example using [webpack](https://webpack.js.org/){:target="_blank"}) and it has extensive [TypeScript](https://www.typescriptlang.org/){:target="_blank"} support.

- First add the Tripetto builder package to your project's `devDependencies`:

```bash
$ npm install {{ site.npm_packages.builder }} -D
```

- Next, import the appropriate symbols into your application (if you use TypeScript, the type information should work out-of-the-box) and you can invoke the builder with a simple command:

```javascript
import { Builder } from "{{ site.npm_packages.builder }}";

// Open the builder directly
Builder.open();

// Or create an instance first
const builder = new Builder();

builder.open();
```

- Depending on your specific project configuration the `ES6` or `ES5` version of the builder might be used.

### Method 2: Using a CDN
If you want to use a more classic approach, you can load the library from a CDN (or host a copy of the library yourself). In this example we use the [unpkg](https://unpkg.com/){:target="_blank"} CDN.

- Add the library to your HTML page and you are ready to go:

[![Try the demo](../../images/demo.svg)](https://codepen.io/tripetto/debug/WzgQLW){:target="_blank"}
[![View the code](../../images/codepen.svg)](https://codepen.io/tripetto/pen/WzgQLW){:target="_blank"}

```html
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/{{ site.npm_packages.builder }}"></script>

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```

- Or use a custom element to host the builder:

[![Try the demo](../../images/demo.svg)](https://codepen.io/tripetto/debug/qYNRPm){:target="_blank"}
[![View the code](../../images/codepen.svg)](https://codepen.io/tripetto/pen/qYNRPm){:target="_blank"}

```html
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/{{ site.npm_packages.builder }}"></script>

        <!-- This div will host the builder -->
        <div id="CustomElement"></div>

        <!-- Fire it up! -->
        <script>
            // Configure a new builder instance
            const builder = new Tripetto.Builder({
                element: document.getElementById("CustomElement")
            });

            // Open the builder
            builder.open();
        </script>
    </body>
</html>

```
