---
source: sections/_guide_runner/01-concepts.md
bookmark: concepts
name: Concepts
title: Concepts
---

The runner handles the start-to-finish process of flowing the respondent through the smart form, typically based on **conditions** met along the way. It does so by presenting the [form definition](../builder/#definitions), which consists of **nodes**, **clusters** and **branches**, one appropriate step at a time during a so-called **instance**. Without us imposing any particular UI. And at the end of this process the supplied user data is returned and you can take it from there.

The runner acts as a finite state machine that handles all the complex logic during the execution of the form. This state machine also emits events to any UI you choose to apply to the form. And because it holds its own state, it has some interesting features like pausing and resuming sessions (instances). And even switching devices.

Also, the runner inherently supports multi-paged forms, even though it is still a purely client-side library. This does require a somewhat different approach for the rendering of the forms. But that particular approach comes with complete UI freedom for you and greatly enhanced form responsiveness because, contrary to traditional form handling, no server round trips are needed once the instance is initiated.

![Runner diagram](../../images/diagrams/runner.svg)

**FYI**, we tend to call the forms you build with Tripetto *smart* because they can contain this advanced logic and conditional flows, allowing for jumps from one part of the form to another or the skipping of certain parts altogether; all depending on the respondent's input.
{: .info }

#### Overview
The following structural diagram shows the aforementioned entities and their respective relationships in a typical basic arrangement. Important to understand is that each cluster in a branch can in turn have branches originating from that cluster. So the following basic structure can recursively repeat itself.

![Form structure](../../images/diagrams/structure.svg)

#### Entities
Before we dive into the implementation of the runner itself we need to define these entities:

#### `Nodes`
A form consists of form elements. These will typically be the form input controls, such as a text input control, dropdown control, etc. In Tripetto we call those elements *nodes*.

#### `Clusters`
One or more *nodes* can be placed in so-called *cluster*. Generally speaking a cluster will render as a page or view. Based on the form logic defined with the builder certain clusters are displayed or just skipped.

#### `Branches`
One or more *clusters* can form a *branch*. A branch can be conditional, meaning it will only be displayed in certain circumstances.

#### `Conditions`
A *branch* can contain one or more *conditions*, which are used to direct flow into the pertaining branch. They are evaluated when a cluster ends. Only subsequent branches with matching condition(s) will be displayed.

#### `Instances`
When a a valid [form definition](../builder/#definitions) is provided to the runner a so-called *instance* can be started. An instance represents a single input/user session. As long as the form is not completed, the related instance remains active. When an instance is started, the first cluster with nodes is automatically displayed. And when eventually there are no more clusters to display, the form is considered complete. The instance is then ended, an appropriate event emitted and the collected form input data provided.

**BTW**, instances can also be paused and resumed later on. In a typical UI-oriented application only one instance at a time can be active. More complex use cases are conceivable, but out of scope of this documentation for now.
{: .info }

#### `Slots`
Data collected with a runner needs to be stored somewhere. Tripetto works with a slot system where each data component is stored in a separate [slot](../blocks/#slots). The slots are defined in the form definition and are directly accessible inside the runner.
