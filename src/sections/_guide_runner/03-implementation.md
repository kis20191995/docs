---
source: sections/_guide_runner/03-implementation.md
bookmark: implementation
name: Implementation
title: Implementing the runner
---

If you successfully installed the runner package in your project you should be able to import the runner namespace:
```typescript
// Import the complete namespace as `Tripetto`
import * as Tripetto from "{{ site.npm_packages.runner }}";

// Or import specific symbols if you prefer
import { RunnerFoundation, Instance } from "{{ site.npm_packages.runner }}";
```

The next step is to implement the runner. There are multiple ways to do this, but the most easy one is as follows:

```typescript
const runner = new RunnerFoundation({
  definition: /** Supply your form definition here */
});

// Listen for some changes
runner.onChange = () => {
  // Do stuff here
};

// Do something with the output when the runner is finished
runner.onFinish = (instance: Instance) => {
  // We're done, export the collected data as CSV
  const csv = Export.CSV(instance);

  console.dir(csv);
};
```

### React example
If you use [React](https://reactjs.org/){:target="_blank"} you can create a runner that simply renders the JSX.

```typescript
// Extend the runner and give it JSX render capabilities!
export class JSXRunner extends Tripetto.RunnerFoundation {
  render(): React.ReactNode {
    return
      this.storyline && (
        <>
          {this.storyline.map((moment: Tripetto.Moment) =>
            moment.nodes.map((node: Tripetto.IObservableNode) => (
              ...
            )
          }
        </>
      );
  }
}
```

Then you can create your component:

```typescript
export class RunnerComponent extends React.PureComponent<{
  definition: IDefinition | string;
}> {
  // Create a new runner instance
  runner = new JSXRunner({
    definition: this.props.definition
  });

  // Listen for changes
  componentDidMount(): void {
    this.runner.onChange = () => {
      // Since the runner has the actual state, we need to update the component.
      // We are good React citizens. We only do this when necessary!
      this.forceUpdate();
    };
  }

  // Render it
  render(): React.ReactNode {
    return this.runner.render();
  }
}
```

### The storyline
The runner generates a storyline that contains all the clusters, nodes and blocks that should be rendered. You can choose from three different operation modes that affect how the storyline is generated:

- `paginated`: Blocks are presented page for page and the user navigates through the pages using the next and back buttons;
- `continuous`: This will keep all past blocks in view as the user navigates using the next and back buttons;
- `progressive`: In this mode all possible blocks are presented to the user. The user does not need to navigate using the next and back buttons (so we can hide those buttons).

Have a look at one of our [demos](../../examples/#runner) to see how the mode effects the runner.

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
