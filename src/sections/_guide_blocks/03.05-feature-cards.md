---
source: sections/_guide_blocks/03.05-feature-cards.md
bookmark: feature-cards
title: Feature cards
sub: true
---

Feature cards are used inside the builder to manage the properties of blocks. They are designed specifically to just show the settings needed in average scenarios and add only the desired additional settings options dynamically on the go. The idea behind this approach is that users only select the block features in the builder they actually want to configure and prevent the typical clutter. Hence the name *feature cards*.

To speed up block development for you, we also supply a list of common features. Those features implement common configuration options, such as the name of the block, description, explanation, etc.

The optional features are listed to the left side of the screen. The configuration options for activated features are presented inside so-called `Cards`, which appear to the right of the feature list

### Creating features and cards
Features are implemented inside a dedicated method of your block decorated with the `@editor` decorator. Inside your node block you can use the collection `this.builder` to supply features. Take a look at the following example. It shows you how to add an optional feature with an empty form card.

```typescript
...
@editor
defineEditor() {
  this.editor.option({
    label: "Example option",
    form: {
      controls: [
        ...
      ]
    }
  });
}
...
```

This will show the feature in the feature list. When a feature is selected, the appropriate form card will be displayed. Next step is to add actual controls to the form card.

### Building feature cards
Tripetto contains a collection of form controls that can be used to build form cards for the feature cards in the builder. The table below shows the available controls. These controls can be used in the `controls` array of the form card.

| Form control         | Description                |
|----------------------|----------------------------|
| `Forms.Button`       | Button form control.       |
| `Forms.Checkbox`     | Checkbox form control.     |
| `Forms.Date`         | Date/time form control.    |
| `Forms.Dropdown`     | Dropdown form control.     |
| `Forms.Email`        | Email form control.        |
| `Forms.HTML`         | HTML form control.         |
| `Forms.Notification` | Notification form control. |
| `Forms.Numeric`      | Numeric form control.      |
| `Forms.Radiobutton`  | Radiobutton form control.  |
| `Forms.Spacer`       | Spacer form control.       |
| `Forms.Static`       | Static form control.       |
| `Forms.Text`         | Text form control.         |
| `Forms.Upload`       | File upload control.       |
| `Forms.Group`        | Form group.                |

#### Example
The following example shows how to use the controls.

```typescript
this.editor.option({
  label: "Example option",
  form: {
    title: "Example",
    controls: [
      new Forms.Text("single").Label("This is a text input"),
      new Forms.Checkbox("This is a checkbox")
    ]
  }
});
```

#### Automatically retrieving data from controls
If you dive deeper into the controls, you will see each control has events you can bind to. One of these events is invoked when the data of the control changes. But there is an easier way to retrieve data from controls by using the bind option of a control. If a control has a `bind` method it supports data two-way data binding. The bind method takes 3 parameters:
- A reference to the object that holds the property the control is going to set;
- The name of the property;
- The default value that's applied as long as the control has no value.

Let's extend our example with a binding.

```typescript
const example = {
    text: "",
    checkbox: false
};

this.editor.option({
  label: "Example option",
  form: {
    title: "Example",
    controls: [
      new Forms.Text(
        "single",
        Forms.Text.bind(example, "text", ""))
          .Label("This is a text input"),
      new Forms.Checkbox(
        "This is a checkbox",
        Forms.Checkbox.bind(example, "checkbox", false))
    ]
  }
});
```

Now the supplied properties `text` and `checkbox` will be automatically updated when the data of the control changes.

### Common features
Common features are out-of-the-box available sets of form controls to manage often used properties. The following common features are available on the `this.builder` instance:

| Feature                    | Explanation                                                 |
|----------------------------|-------------------------------------------------------------|
| `groups`          | Contains common group labels.                             |
| `name`            | Manages the name of a node.                                    |
| `description`     | Manages the description of a node.                             |
| `placeholder`     | Manages the placeholder of a node.                             |
| `explanation`     | Manages the explanation of a node.                             |
| `visibility`      | Manages the visibility of a node.                              |
| `required`        | Manages the slot requirements.                                 |
| `alias`           | Manages the slot alias.                                        |
| `transformations` | Manages the transformation for a `TextSlot`.                   |
| `collection`      | Manages a [collection](#collections). |

#### Example
Common features can be added directly to your `@editor` method.

```typescript
...
@editor
defineEditor() {
    this.editor.name();
    this.editor.description();
    this.editor.explanation();

    this.editor.groups.options();
    this.editor.visibility();
}
...
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
